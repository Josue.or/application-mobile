package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import static data.Country.countries;

public class DetailFragment extends Fragment {

    TextView itemLanguage;
    TextView itemCapital;
    TextView itemPopulation;
    TextView itemSuperficie;
    TextView itemCurrency;
    TextView itemCountry;
    ImageView itemImage;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }



    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemCurrency = view.findViewById(R.id.data_monnaie);
        itemLanguage = view.findViewById(R.id.data_langue2);
        itemCapital = view.findViewById(R.id.data_capitale);
        itemImage = view.findViewById(R.id.data_image);
        itemCountry = view.findViewById(R.id.data_country);
        Context c = itemImage.getContext();

        itemPopulation = view.findViewById(R.id.data_population);
        itemSuperficie = view.findViewById(R.id.data_superficie);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int countryId = args.getCountryId();
        itemCurrency.setText(countries[countryId].getCurrency());
        itemLanguage.setText(countries[countryId].getLanguage());
        itemCapital.setText(countries[countryId].getCapital());
        itemCountry.setText(countries[countryId].getName());
        itemPopulation.setText(String.valueOf(countries[countryId].getPopulation()));
        itemSuperficie.setText(String.valueOf(countries[countryId].getArea()));
        itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier (countries[countryId].getImgUri(), null , c.getPackageName())));
        view.findViewById(R.id.retour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}